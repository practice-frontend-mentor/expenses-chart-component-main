//Obteniendo datos del JSON
const dayAmount=`[
    {
      "day": "mon",
      "amount": 17.45
    },
    {
      "day": "tue",
      "amount": 34.91
    },
    {
      "day": "wed",
      "amount": 52.36
    },
    {
      "day": "thu",
      "amount": 31.07
    },
    {
      "day": "fri",
      "amount": 23.39
    },
    {
      "day": "sat",
      "amount": 43.28
    },
    {
      "day": "sun",
      "amount": 25.48
    }
  ]`
const jsonData=JSON.parse(dayAmount);
  
//Obeteniendo HTML
  let totalAmountTag=document.getElementById('totalAmount');
  let mon=document.getElementById('mon');
  let tue=document.getElementById('tue');
  let wed=document.getElementById('wed');
  let thu=document.getElementById('thu');
  let fri=document.getElementById('fri');
  let sat=document.getElementById('sat');
  let sun=document.getElementById('sun');
  let daysCont=[mon,tue,wed,thu,fri,sat,sun]


  let amountDll=[];
  jsonData.forEach(amount => {
    amountDll.push(amount.amount)  
  });
  let totalAmount = amountDll.reduce((act,currValue)=>act+currValue);

  let hundredPercent=Math.max(...amountDll)

//Seteando valores en el DOM
  totalAmountTag.innerHTML=totalAmount.toFixed(2);
  //Asignando valor a las barras
  daysCont.forEach(day=>{
    let actualPercent;
    let barra=day.firstChild.nextSibling;
    
    //Interactividad de barras
    barra.addEventListener('mouseover',()=>{
      barra.style.opacity='.7';
      day.firstChild.style.display='block';
    });

    barra.addEventListener('mouseout',()=>{
      barra.style.opacity='1';
      day.firstChild.style.display='none';
    })

    //Asignando el valor del letrero y altura de la barra
      switch(day.lastChild.innerText){
        case 'mon':
          day.firstChild.innerText=jsonData[0].amount
          actualPercent=((jsonData[0].amount)*100)/hundredPercent
          barra.style.height=actualPercent + 'px';
          if(jsonData[0].amount==hundredPercent){
            barra.style.backgroundColor='var(--softCyan)'; 
          }
        break;
        case 'tue':
          day.firstChild.innerText=jsonData[1].amount
          actualPercent=((jsonData[1].amount)*100)/hundredPercent
          barra.style.height=actualPercent + 'px';
          if(jsonData[1].amount==hundredPercent){
            barra.style.backgroundColor='var(--softCyan)'; 
          }
        break;
        case 'wed':
          day.firstChild.innerText=jsonData[2].amount
          actualPercent=((jsonData[2].amount)*100)/hundredPercent
          barra.style.height=actualPercent + 'px';
          if(jsonData[2].amount==hundredPercent){
           barra.style.backgroundColor='var(--softCyan)'; 
          }
        break;
        case 'thu':
          day.firstChild.innerText=jsonData[3].amount
          actualPercent=((jsonData[3].amount)*100)/hundredPercent
          barra.style.height=actualPercent + 'px';
          if(jsonData[3].amount==hundredPercent){
            barra.style.backgroundColor='var(--softCyan)'; 
          }
        break;
        case 'fri':
          day.firstChild.innerText=jsonData[4].amount
          actualPercent=((jsonData[4].amount)*100)/hundredPercent
          barra.style.height=actualPercent + 'px';
          if(jsonData[4].amount==hundredPercent){
            barra.style.backgroundColor='var(--softCyan)'; 
          }
        break;
        case 'sat':
          day.firstChild.innerText=jsonData[5].amount
          actualPercent=((jsonData[5].amount)*100)/hundredPercent
          barra.style.height=actualPercent + 'px';
          if(jsonData[5].amount==hundredPercent){
            barra.style.backgroundColor='var(--softCyan)'; 
          }
        break;
        case 'sun':
          day.firstChild.innerText=jsonData[6].amount
          actualPercent=((jsonData[6].amount)*100)/hundredPercent
          barra.style.height=actualPercent + 'px';
          if(jsonData[6].amount==hundredPercent){
            barra.style.backgroundColor='var(--softCyan)'; 
          }
        break;
      }
  });